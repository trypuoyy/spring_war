package com.example.spring_war;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HelloWorld {

    @GetMapping("/")
    public String helloMessage(Model model) {
        model.addAttribute("message", "Spring War!");
        return "index";
    }

}
