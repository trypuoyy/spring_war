package com.example.spring_war;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringWarApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringWarApplication.class, args);
    }

}
